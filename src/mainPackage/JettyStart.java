package mainPackage;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.webapp.WebAppContext;

public class JettyStart {

	public static void main(String[] args) {
		Server server = new Server(8081);
		WebAppContext ctx = new WebAppContext("src/mainPackage", "/");
		ctx.setServer(server);
		server.setHandler(ctx);
		try {
			server.start();
			server.join();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}
}

// Server server = new Server(8080);
// server.setHandler(new AbstractHandler() {
//
// public void handle(String target, Request baseRequest, HttpServletRequest
// request, HttpServletResponse response)
// throws IOException, ServletException {
// PrintWriter writer = response.getWriter();
// if("/test".equals(target)) {
// writer.println("<strong>test</strong>");
//
// }
// baseRequest.setHandled(true);
// response.setHeader("content-type", "text/html");
// System.out.println(target);
// writer.println("<strong>Hello</strong>");
//
// }
// });
// try {
// server.start();
// server.join();
// } catch (Exception e) {
// e.printStackTrace();
// }
