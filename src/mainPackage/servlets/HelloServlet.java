package mainPackage.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
public class HelloServlet extends HttpServlet {
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String val = req.getParameter("item").toString();
		System.out.println(val);
		PrintWriter writer = resp.getWriter();
		resp.setHeader("content-type", "text/html");
		writer.println(val);
		resp.setStatus(HttpServletResponse.SC_OK);
	}
	
}

	
	// request, HttpServletResponse response)
			// throws IOException, ServletException {
			// PrintWriter writer = response.getWriter();
			// if("/test".equals(target)) {
			// writer.println("<strong>test</strong>");
			//
			// }


