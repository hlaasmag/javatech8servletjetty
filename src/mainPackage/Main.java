package mainPackage;

import java.io.File;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.apache.logging.log4j.core.layout.PatternLayout;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class Main {

	private static final Logger LOG = LogManager.getLogger(Main.class); // needs to be static.
																		
	public static void main(String[] args) throws IOException {
		Document doc = Jsoup.connect("http://www.delfi.ee/").get();
		File file = new File("delfiHeadlines.txt");
		boolean append = true;

		PrintWriter outFile = new PrintWriter(new FileWriter(file, append));

		for (Element title : doc.select(".article-title")) {
			if (FileUtils.readFileToString(file).contains(title.text())) {
				continue;
			} else {
				LOG.debug("Lisasin pealkirja: " + title.text() + "] [" + title.attr("href"));
				// System.out.println("Lisasin pealkirja: " + title.text());
				outFile.println(LocalDateTime.now());
				outFile.println(title.text());
				outFile.println(title.attr("href"));
				outFile.println();
				outFile.println();
			}
		}
		outFile.flush();
		outFile.close();
		System.out.println("Finished fetching headlines and URL-s from delfi.ee.");
	}
}
